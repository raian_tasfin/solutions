def solve(m, n):
	if m > n:
		m, n = n, m
	
	ans = m*n
	if m > 2:
		return ans/2 + ans % 2
	
	if m == 2:
		return int((n+3)/4)*4 - ( 2 if n % 4 == 1 else 0 )

	return ans		

T = int(input())
for t in range(1, T+1):
	x, y = [int(x) for x in input().split()]
	
	print("Case %d: %d" % (t, solve(x, y)))
