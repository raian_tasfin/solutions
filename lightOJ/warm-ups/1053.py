T = int(input())
for t in range(1, T+1):
	a, b, c = sorted([int(x) for x in input().split()])
	print("Case %d: %s" % (t, "yes" if a**2+b**2==c**2 else "no"))
