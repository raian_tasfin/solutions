from collections import deque
import bisect

factorialList = [ 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600, 6227020800, 87178291200, 1307674368000, 20922789888000, 355687428096000, 6402373705728000, 121645100408832000 ]
arr = deque([])

def solve(n):
	global arr
	global factorialList

	arr = deque([])
	upperBound = bisect.bisect_right(factorialList, n)
	for i in reversed(range(0, upperBound)):
		if n >= factorialList[i]:
			n -= factorialList[i]
			arr.appendleft(i)
	return n == 0

T = int(input())
for t in range(1, T+1):
	ans = solve(int(input()))
	print("Case %d: " % t , end = '')
	if ans:
		print("%d!" % arr.popleft(), end='')
		while arr:
			print("+%d!" % arr.popleft(), end = '')
		print()
	else:
		print("impossible")
