/*
 * Palindromic Numbers (II)
 * https://lightoj.com/problem/palindromic-numbers-ii
*/

T = int(input())

for t in range(1,T+1):
	num = input()
	print("Case %d:" %(t), end=" " )
	if num == num[::-1]:
		print("Yes")
	else:
		print("No")

