T = int(input())

for t in range(0, T):
	a = int(input())
	if a>10:
		print("%d %d" % (10, a-10))
	else:
		print("%d %d" % (0, a))
