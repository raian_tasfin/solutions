T = int(input())
for t in range(1, T+1):
	n = int(input())
	mp = {}
	arr = []

	for i in range(0, n):
		inputList = input().split()
		name = inputList[0]
		vol = 1
		for j  in inputList[1:]:
			vol *= int(j)		
		mp[vol] = name
		arr.append(vol)
	arr.sort()

	if arr[0] <	 arr[n-1]:
		print("Case %d: %s took chocolate from %s" % (t, mp[arr[n-1]], mp[arr[0]] ))
	else:
		print("Case %d: no thief" % (t))
        