def cntFromOne(n):
	if n < 1:
		return 0
	return int(2*n/3)

T = int(input())
for t in range(1, T+1):
	a, b = [int(x) for x in input().split()]
	print("Case %d: %d" % (t, cntFromOne(b)-cntFromOne(a-1)))
