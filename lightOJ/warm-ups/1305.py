T  = int(input())
for t in range(1, T+1):
	ax, ay, bx, by, cx, cy = [int(x) for x in input().split()]
	
	bxLocal = bx-ax
	byLocal = by-ay
	cxLocal = cx-ax
	cyLocal = cy-ay

	area = abs(bxLocal*cyLocal - byLocal*cxLocal)

	dxLocal = cx-bx
	dyLocal = cy-by

	dx = dxLocal+ax
	dy = dyLocal+ay

	print("Case %d: %d %d %d" % (t, dx, dy, area))
