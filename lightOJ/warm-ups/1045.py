import math

arr = [0]

for i in range(1, 1000001):
	arr.append(arr[i-1] + math.log(i))

def solve(n, b):
	return int(arr[n] / math.log(b) + 1)

T = int(input())
for t in range(1, T+1):
	n, b = [int(x) for x in input().split()]

	print("Case %d: %d" % (t, solve(n, b)))
