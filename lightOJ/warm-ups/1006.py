MOD = 10000007
curr = cycle = tmp = 0
arr = []

cnt = 6

def solve(n):
	if n < 6:
		return arr[n] % MOD

	cycle = int(n/6)

	for x in range(0, cycle):
		for i in reversed(range(0, 5)):
			arr[i] += arr[i+1]
			arr[i] %= MOD
		
		curr = 0
		for i in range(0, 6):
			arr[i] += curr
			curr += arr[i]
			arr[i] %= MOD
			curr %= MOD
	
	return arr[n%6] % MOD

T = int(input())

for t in range(1, T+1):
	inputList = [int(x) for x in input().split()]

	arr = inputList[:6]
	n = inputList[6]

	print("Case %d: %d" % (t, solve(n)))
