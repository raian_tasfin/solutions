import math

C= 4 - 2 * math.acos(0.0) 

T = int(input())
for t in range(1, T+1):
	r = float(input())

	print("Case %d: %0.2f" % (t, r**2 * C ))
