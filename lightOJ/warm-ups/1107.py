T = int(input())
for t in range(1, T+1):
	a, b, c, d = [int(x) for x in input().split()]
	n = int(input())
	print("Case %d:" % (t))
	for i in range(0, n):
		x, y = [int(x) for x in input().split()]
		if a < x < c and b < y < d:
			print("Yes")
		else:
			print("No")
