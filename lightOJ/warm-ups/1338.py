T = int(input())
for t in range(1, T+1):
	first = sorted(input().lower())
	second = sorted(input().lower())

	firstBegin = 0
	while first[firstBegin] == ' ':
		firstBegin++

	secondBegin = 0
	while second[secondBegin] == ' ':
		secondBegin++

	if first[firstBegin:] == second[secondBegin:]:
		print("Case %d: Yes" % (t) )
	else:
		print("Case %d: No" % (t) )
