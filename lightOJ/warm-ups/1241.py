T = int(input())
for t in range(1, T+1):
	n = int(input())
	arr = [int(x) for x in input().split() ]
	prev = 2
	cnt = 0
	for curr in arr:
		dif = curr - prev
		if dif > 0:
			cnt += int(dif/5)
			if dif % 5 > 0:
				cnt += 1
		prev = curr
	print("Case %d: %d" % (t, cnt))
