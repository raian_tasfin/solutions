import math

T = int(input())
for t in range(1, T+1):
	ox, oy, ax, ay, bx, by = [float(x) for x in input().split()]

	ax -= ox
	ay -= oy
	bx -= ox
	by -= oy

	d = math.sqrt((ax-bx)**2 + (ay-by)**2)
	r = math.sqrt(ax**2 + ay**2)

	angl = math.acos(1 - 0.5 * d**2 * (1/r**2) )

	print("Case %d: %0.10f" % (t, r*angl))
