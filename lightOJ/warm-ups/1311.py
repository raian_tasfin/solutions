T = int(input())
for cs in range(1, T+1):
	v1, v2, v3, a1, a2 = [float(x) for x in input().split()]
	
	t = max(v1/a1, v2/a2)
	d = v1**2/(2*a1) + v2**2/(2*a2)
	s = v3*t

	print("Case %d: %0.10f %0.10f" % (cs, d, s))
