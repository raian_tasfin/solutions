T = int(input())
for t in range(1, T+1):
	n, m  = [int(x) for x in input().split()]
	arr = [int(x) for x in input().split()]
	for i in range(0, m):
		inputList = input().split()
		if inputList[0] == 'S':
			arr = [x+int(inputList[1]) for x in arr]
		elif inputList[0] == 'M':
			arr = [x*int(inputList[1]) for x in arr]
		elif inputList[0] == 'D':
			arr = [int(x/int(inputList[1])) for x in arr]
		elif inputList[0] == 'P':
			arr[int(inputList[1])], arr[int(inputList[2])] = arr[int(inputList[2])], arr[int(inputList[1])]
		else:
			arr = list(reversed(arr))
	print("Case %d:" % t)
	print(*arr,  sep = ' ')
