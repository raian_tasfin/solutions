def colour(r, c):
	return r % 2 == c % 2

def solve(r, c, rr, cc):
	if colour(r, c) != colour(rr, cc):
		return -1
	if abs(r-rr) == abs(c-cc):
		return 1
	return 2

T = int(input())
for t in range(1, T+1):
	r, c, rr, cc = [int(x) for x in input().split()]
	ans = solve(r, c, rr, cc)
	print("Case %d: %s" % (t, "impossible" if ans == -1 else str(ans)))
