T = int(input())
for t in range(1, T+1):
	first = input().split('.')
	second = input().split('.')

	for i in range(0, len(second)):
		second[i] = str(int(second[i], 2))

	print("Case %d: " % (t), end='')
	if first == second:
		print("Yes")
	else:
		print("No")
