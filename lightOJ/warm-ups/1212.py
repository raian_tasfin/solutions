from collections import deque

T = int(input())
for t in range(1, T+1):
	print("Case %d:" % t)
	n, m = [int(x) for x in input().split()]
	dq = deque()
	for i in range(0, m):
		inputList = input().split()
		command, num = inputList[0], 0 if len(inputList) == 1 else inputList[1]
		ans = ""
		if command[1] == 'u':
			if len(dq) == n:
				ans = "The queue is full"
			else:
				ans = "Pushed in "
				if command[4] == 'L':
					dq.appendleft(num)
					ans += "left: "
				else:
					dq.append(num)
					ans += "right: "
				ans += num
		else:
			if not dq:
				ans = "The queue is empty"
			else:
				ans = "Popped from "
				if command[3] == 'L':
					ans += "left: " + dq.popleft()
				else:
					ans += "right: " + dq.pop()
		print(ans)
