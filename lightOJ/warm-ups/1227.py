T = int(input())
for t in range(1, T+1):
	n, p, q = [int(x) for x in input().split()]
	cnt = sm = 0
	lst = [int(x) for x in input().split()]
	for var in lst:
		if sm + var <= q and cnt+1 <= p:
			sm += var
			cnt += 1
	print("Case %d: %d" % (t, cnt))
