T = int(input())
for t in range(1, T+1):
	yourPos, liftPos = [int(x) for x in input().split()]
	print("Case %d: %d" % (t, 19+4*(abs(yourPos-liftPos)+yourPos)))
