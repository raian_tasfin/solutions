import math

x = y = 0

def solve(n):
	global x, y
	sq = int(math.sqrt(n))

	if sq**2 == n:
		x = 1
		y = sq
		if sq % 2 == 0:
			x, y = y, x
		return

	sq += 1
	team = sq**2

	x = team - n + 1
	y = sq

	if x > sq:
		tmp = x
		x = sq
		y -= (tmp - sq)
	if sq % 2 == 0:
		x, y = y, x

T = int(input())
for t in range(1, T+1):
	n = int(input())
	
	solve(n)
	print("Case %d: %d %d" % (t, x, y))
