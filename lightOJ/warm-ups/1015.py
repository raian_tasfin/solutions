T = int(input())
for t in range(1, T+1):
	input()
	n = int(input())
	arr = input().split()
	sm = 0
	for it in arr:
		sm += max(0, int(it))
	
	print("Case %d: %d" % (t, sm))
