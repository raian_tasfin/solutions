T = int(input())
for t in range(1, T+1):
	print("Case %d:"  % (t))
	total = 0
	n = int(input())
	for i in range(0, n):
		st = input().split()
		if st[0] == "donate":
			total += int(st[1])
		else:
			print(total)
