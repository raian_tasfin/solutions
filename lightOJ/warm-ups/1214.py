T = int(input())
for t in range(1, T+1):
	a, b = [int(x) for x in input().split()]
	print("Case %d: %sdivisible" % (t, "not " if a % b != 0 else ""))
