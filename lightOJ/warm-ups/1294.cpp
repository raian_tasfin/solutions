#include <bits/stdc++.h>
using namespace std;
const char newline = '\n';

int main(){
    long long T, t=1, m, n;
    cin>> T;
    while(T--){
        cin>> n >> m;
        cout<< "Case " << (t++) << ": " << n*m/2 << newline;
    }
    
    return 0;
}
