T = int(input())
for t in range(1, T+1):
	print("Case %d: %s" % (t, "even" if bin(int(input()))[2:].count('1') % 2 == 0 else "odd" ))
