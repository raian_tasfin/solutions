T = int(input())
for t in range(1, T+1):
	n = int(input())
	bn = list(bin(n)[2:][::-1]+'0')

	ln = len(bn)
	zeroes = 0
	ones = 0
	for i in range(0, ln-1):
		if bn[i] == '0':
			zeroes += 1
		else:
			ones += 1
	
		if bn[i] == '1' and bn[i+1] == '0':
			ones -= 1
			bn[i], bn[i+1] = bn[i+1], bn[i]
			i -= 1
			while zeroes != 0:
				bn[i] = '0'
				i -= 1
				zeroes -= 1
			while ones != 0:
				bn[i] = '1'
				i -= 1
				ones -= 1
			break
	print("Case %d: %d" % (t, int(''.join(bn)[::-1], 2)))
